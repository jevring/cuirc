import javax.swing.*;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.*;
import java.util.LinkedList;
import java.util.List;
import java.awt.*;
import java.io.StringWriter;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-03 18:05:16
 */
public class ChannelPane {
    private void go() {
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setLayout(new BorderLayout());
        JTextPane channel = new JTextPane();
        channel.setPreferredSize(new Dimension(500, 500));
        JScrollPane sp = new JScrollPane();
        sp.setViewportView(channel);
        channel.setSize(500, 500);
        window.add(sp, BorderLayout.CENTER);
        window.pack();
        window.setVisible(true);
        //window.setSize(500, 500);
        window.setLocation(500, 500);


        // here comes the experiments
        channel.setEditable(false);
        channel.setBackground(Color.DARK_GRAY);
        final Document document = channel.getDocument();
        Style nickStyle = ((StyledDocument) document).addStyle("nick", null);
        StyleConstants.setBold(nickStyle, true);
        StyleConstants.setFontFamily(nickStyle, "SansSerif");
        StyleConstants.setFontSize(nickStyle, 12);
        StyleConstants.setForeground(nickStyle, Color.YELLOW);

        Style textStyle = ((StyledDocument) document).addStyle("text", null);
        StyleConstants.setBold(textStyle, false);
        StyleConstants.setFontFamily(textStyle, "SansSerif");
        StyleConstants.setFontSize(textStyle, 12);
        StyleConstants.setForeground(textStyle,  Color.WHITE);

        //channel.setContentType("text/html");
/*
        channel.addHyperlinkListener(new HyperlinkListener() {

            public void hyperlinkUpdate(HyperlinkEvent e) {
                System.out.println("hyperlink event!: " + e);
            }
        });
*/        

        TabStop nickTabStop = new TabStop(150.0f, TabStop.ALIGN_RIGHT, TabStop.LEAD_NONE);
        TabStop textTabStop = new TabStop(150.0f, TabStop.ALIGN_LEFT, TabStop.LEAD_NONE);
        TabStop[] tss = new TabStop[2];
        tss[0] = nickTabStop;
        tss[1] = textTabStop;
        TabSet ts = new TabSet(tss);

        Style logicalStyle = channel.getLogicalStyle();
        StyleConstants.setTabSet(logicalStyle, ts);
        channel.setLogicalStyle(logicalStyle);
        

        // todo: see if we can't use PicoContainers here, like in IDEA: for practise if nothing else

        // _todo: how do I get the nicks on the left side of this tab stop and the text on the right side?
        // by prefixing the text with tabs, pushing them to the correct tab stop:
        // http://www.java2s.com/Code/Java/Swing-JFC/ATabSetinaJTextPane.htm
        try {
            //document.insertString(document.getLength(), s, textStyle);
            //System.out.println(document.getEndPosition());
            for (String s : conversation) {
                document.insertString(document.getLength(), s + "\n", textStyle);
            }
/*
            for (int i = 0; i < 15; i++) {
                //document.insertString(document.getLength(), s, textStyle);
                document.insertString(document.getLength(), "\t<fizzy bubbelish!>", nickStyle);
                document.insertString(document.getLength(), "\tThis is me saying something, bitches!\n", textStyle);
                document.insertString(document.getLength(), "\t<simian>", nickStyle);
                document.insertString(document.getLength(), "\tcheck out the monkeys at http://www.google.com !!!!!!!!!!\n", textStyle);
                channel.setCaretPosition(document.getLength());
                try {
                    // todo: why do the lines overlap when we sleep in between each addition?
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
*/            

            System.out.println(document.getText(0, document.getLength()));
            //System.out.println(document.getEndPosition());
        } catch (BadLocationException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        new ChannelPane().go();
    }

    String[] conversation = {"[23:02:40] <+EEKie> a dead body?",
"[23:02:42] [WHO] DONE #efnet",
"[23:02:42] <+Braudan> it is obviously a subsidized (or better redistributed) system",
"[23:02:44] [MODE] moderatum sets +vv Mug captain_ on #politics",
"[23:02:49] <+davtek> guy parked his rv and passed away...two weeks later",
"[23:02:55] <+davtek> his dog died in there too",
"[23:02:57] <+h3inrich> Braudan: your GKV isnt gov't run ?",
"[23:03:01] <+Braudan> nope",
"[23:03:19] <+Braudan> they're all private companies, some public some private", 
"[23:03:20] <+davtek> sad stories man...lonely ppl dieing long the side of the road",
"[23:03:31] <+davtek> your gonna hear alot more stories like that..",
"[23:03:36] <+davtek> trust me",
"[23:03:58] <+Braudan> so even when you are publicly insured you get to choose an insurance company (and back in the day that even mattered)",
"[23:04:40] <+Braudan> it sucks, but still better than 40mln uninsured i guess",
"[23:04:41] <+EEKie> and what makes you trust worthy?",
"[23:05:17] <+davtek> life experiences and my proof of 'trust worthyness'",
"[23:05:19] <+davtek> :P",
"[23:05:20] <+bambip> i'll vouch for him",
"[23:05:27] <+davtek> i dont lie...",
"[23:05:57] <+bambip> he's a techie",
"[23:06:03] <+SweetLeaf> Liar I said Liar",
"[23:06:04] <+SweetLeaf> heh",
"[23:06:05] <+davtek> what can you toss back in my face that i was wrong about?",
"[23:06:19] <+SweetLeaf> hey dave spit",
"[23:06:26] <+davtek> and..when i am...i'm prob the first to admit it",
"[23:06:32] <+RustyFord> http://childhoodsend.net/ <--- My friends Pink Floyd tribute band",
"[23:06:32] <+SweetLeaf> make sure which way to throw it",
"[23:06:42] <+davtek> cus i get all freaked out and check my facts",
"[23:06:53] <+bambip> do you have a clearance, davtek?",
"[23:06:58] <+SweetLeaf> im broke now",
"[23:07:06] <+SweetLeaf> its fun spending money",
"[23:07:15] <+SweetLeaf> but a downer when its gone",
"[23:07:29] <+davtek> i have",
"[23:07:29] <+Braudan> nah",
"[23:07:31] <+SweetLeaf> new tires and a new ham radio",
"[23:07:33] <+Braudan> spending money is gay",
"[23:07:38] <+EEKie> being wrong on facts does not make you a liar, you're suffering the same bush derangement symptoms the lefties are sufferring",
"[23:07:45] <+davtek> i require...ummmm",
"[23:07:47] <+Braudan> I'm plagued by guilt whenever i buy something",
"[23:07:49] <+Braudan> unless its food",
"[23:07:49] <+Braudan> :P",
"[23:07:52] <+SweetLeaf> Liar I said Liar",
"[23:07:52] <+bambip> see, he is trustworthy",
"[23:07:57] <+davtek> waivers now..being civillian",
"[23:08:11] <+SweetLeaf> pelosi lied",
"[23:08:23] <+bambip> just like a digital certificate",
"[23:08:36] <+davtek> man..has she been keepin a low profile lately..or what???",
"[23:08:37] <+davtek> hahaha",
"[23:08:41] <+bambip> from berisign",
"[23:08:48] <+bambip> verisign",
"[23:08:51] <+SweetLeaf> Pelosi Lied",
"[23:08:51] [MODE] moderatum sets +l 192 on #politics",
"[23:09:16] [JOIN] bauhaus (st_lush@99-37-165-110.lightspeed.hstntx.sbcglobal.net)",
"[23:09:34] <+davtek> hahah..verisign is prob the most prolific porn hosting provider",
"[23:09:36] <+davtek> hahah",
"[23:09:40] <+hadassah> pelosi is ole P backwards",
"[23:09:44] [MODE] moderatum sets +v bauhaus on #politics",
"[23:10:24] [*] +davtek gasps",
"[23:10:32] <+davtek> is nancy jewish?",
"[23:10:36] <+davtek> humm",
"[23:10:39] <+davtek> no way",
"[23:10:42] <+davtek> let me look",
"[23:10:58] <+TWThoma> Nancy Pelosi scares me",
"[23:11:23] <+TWThoma> Braudan : I don't get guilty when I buy something",
"[23:11:50] <+Braudan> good for you",
"[23:11:50] <+Braudan> :)",
"[23:12:03] <+davtek> ha",
"[23:12:05] <+TWThoma> *sings* Chicago Niagra Falls",
"[23:12:09] <+davtek> catholic all girls school",
"[23:12:12] <+davtek> just as bad",
"[23:12:18] <+Braudan> there's nothing better than watching your bank account grow fat tho",
"[23:12:19] <+Braudan> :P",
"[23:12:28] [*] +davtek looks at lexes",
"[23:12:42] <+TWThoma> my bank account may grow fat soon",
"[23:12:50] <+TWThoma> and I'll really be back in business",
"[23:12:57] <+dyrc`huh> how about spending money? that sure beats saving money",
"[23:13:03] <+raywork> dyrc`huh: wrong",
"[23:13:09] <+Braudan> what raywork said"};
}

package cu.irc.gui;

import cu.irc.commands.CommandHandler;
import cu.irc.commands.ErrorHandler;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

/**
 * This panel is a default I/O pane. It has a large window for output, and a small text field at the base for input.
 * To create a channel, see {@link cu.irc.gui.ChannelPane}, which has a nick list in BorderLayout.EAST.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-07 22:07:48
 */
public abstract class IOPane extends JInternalFrame implements ErrorHandler {
    private final Style style;
    private final DateFormat timestamp = new SimpleDateFormat("[HH:mm:ss]");
    private final JTextPane output;
    private final JTextField input;
    protected final CommandHandler commandHandler;
    /**
     * Keeps a list of the history of the messages sent (maybe limit to a certain number?). todo: when a new limit is set that is shorter than the current one, this should be trimmed.
     * When a command from the history is executed, that command should be moved from its
     * position in history and made the first one. If a command is merely used as a starting
     * point, don't move the command in the history.
     * todo: register up and down arrows to move through this history.
     * todo: also have a way of showing it, like "history" in linux
     * todo: add a "grep" command to let you search through it
     * todo: add a ctrl+r to do a "reverse-i-search" through the history, just like bash
     * todo: in fact, see how bash has solved history
     */
    private final List<String> history = new LinkedList<String>();

    protected final Color paleblue = new Color(164,209,255); // used for other's nicks
    protected final Color darkerblue = new Color(81,101,128); // used for the background
    protected final Color yellow = new Color(255,255,0); // used in clean for my nick and for mode tokens
    protected final Color green = new Color(0,255,0); // used in clean for WHOIS and MODE and stuff

    protected Color foreground = Color.WHITE;
    
    public IOPane(String title, CommandHandler commandHandler) {
        super(title, true, true, true, true);
        this.commandHandler = commandHandler;
        setLayout(new BorderLayout());

        // output
        output = new JTextPane();
        output.setEditable(false);
        JScrollPane sp = new JScrollPane(output);
        add(sp, BorderLayout.CENTER);

        input = new JTextField();
        add(input, BorderLayout.SOUTH);

        input.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                submitInput();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_FOCUSED);
        // todo: on focus we should set the cursor in the input box
/* This is likely to fuck up selection in the text window, something we must support.        
        output.addFocusListener(new FocusListener() {

            public void focusGained(FocusEvent e) {
                input.requestFocusInWindow();
            }

            public void focusLost(FocusEvent e) {
            }
        });
*/
        
        final Document document = output.getDocument();
        output.setBackground(darkerblue);
        style = ((StyledDocument) document).addStyle("default", null);
        StyleConstants.setBold(style, false);
        StyleConstants.setFontFamily(style, "Verdana"); // SansSerif
        StyleConstants.setFontSize(style, 12);

        input.setBackground(darkerblue);
        input.setForeground(Color.WHITE);
        input.setCaretColor(Color.WHITE);

        // todo: it would be REALLY nice if we could read MTS themes

    }

    protected void appendln(String s) {
        appendln(s, foreground);
    }

    protected void append(String s) {
        append(s, foreground);
    }

    protected void appendln(String s, Color c) {
        append(s + "\n", c);
    }

    protected void append(String s, Color c) {
        StyleConstants.setForeground(style, c);
        try {
            final Document document = output.getDocument();
            // note: we can't default timestamp here, since we might want to construct longer lines,
            // and we only want to timestamp at the beginning
            document.insertString(document.getLength(), s, style);
            output.setCaretPosition(document.getLength());
        } catch (BadLocationException e) {
            // we can't get here, because we know that .getLength() will always be valid
            throw new AssertionError(e);
        }
    }


    /**
     * Submits the data currently in the input field to the underlying connection.
     */
    private void submitInput() {
        // todo: add doskey functionality
        /*
        Maybe we can use a JX-things for that. Something with auto-complete and a hidden list component that keeps track of the history
         maybe we don't want auto-complete though
         */
        String s = input.getText();
        if (s != null && s.length() > 0) {
            commandHandler.handleCommand(s, this, this);
            input.setText("");
        }
    }

    public void handleError(String message, Throwable t) {
        append(getTimestamp() + " ");
        appendln(message);
        append(getTimestamp() + " ");
        appendln(t.toString());
    }

    public void handleError(String message) {
        append(getTimestamp() + " ");
        appendln(message);
    }

    /**
     * Gets a timestamp based on a common format.
     * @return a timestamp based on the current timestamp format.
     */
    protected String getTimestamp() {
        return getTimestamp(System.currentTimeMillis());
    }

    /**
     * Gets a timestamp for the provided time.
     * @param time the time in milliseconds.
     * @return a timestamp based on the current timestamp format. 
     */
    protected String getTimestamp(long time) {
        return timestamp.format(new Date(time));
    }
}

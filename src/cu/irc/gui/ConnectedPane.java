package cu.irc.gui;

import cu.irc.net.ServerConnection;

/**
 * Implementing this interface makes external access to your {@link cu.irc.net.ServerConnection} possible.
 *
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-08 21:47:20
 */
public interface ConnectedPane {

    public ServerConnection getServerConnection();
}

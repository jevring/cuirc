package cu.irc.gui;

/**
 * Implementers of this interface manage windows.
 *
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-10 17:48:20
 */
public interface WindowManager {

    /**
     * Adds a window to the desktop and to the tree.
     * @param window the window to add.
     */
    public void addWindow(IOPane window);

    /**
     * Removes a window from the desktop and from the tree.
     *
     * @param window the window to remove.
     */
    public void removeWindow(IOPane window);
}

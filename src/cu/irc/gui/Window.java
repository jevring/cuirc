package cu.irc.gui;

import java.util.List;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-10 17:57:05
 */
public interface Window {
    public List<Window> getChildren();
}

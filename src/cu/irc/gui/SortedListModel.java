package cu.irc.gui;


import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListDataEvent;
import java.util.*;

/**
 * A ListModel that keeps the entries of the model sorted.
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-11 21:32:10
 * @param <T> content type
 */
public class SortedListModel<T extends Comparable> implements ListModel {
    private final Set<ListDataListener> changeListeners = new HashSet<ListDataListener>();
    private final List<T> items = new ArrayList<T>();

    public int getSize() {
        return items.size();
    }

    public T getElementAt(int index) {
        return items.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        changeListeners.add(l);
    }

    public void removeListDataListener(ListDataListener l) {
        changeListeners.remove(l);
    }

    /**
     * Batch add a large number of entries. This will fire only one update (for the whole list),
     * as opposed to one update for each item.
     *
     * @param items the items to add
     */
    public void add(T[] items) {
        this.items.addAll(Arrays.asList(items));
        // note: in some cases it might be better to insert them one by one (if the existing list is very large compared to the passed list).
        // we can change that later, though. Since this will mostly be a first-population kind of thing, it shouldn't be a problem
        Collections.sort(this.items);
        // update everything
        final ListDataEvent listDataEvent = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0, this.items.size());
        for (ListDataListener listDataListener : changeListeners) {
            listDataListener.intervalRemoved(listDataEvent);
        }
    }

    /**
     * Adds an item to the list model in a sorted fashion.
     * @param item the item to add
     */
    @SuppressWarnings("unchecked") // we can surpress because we know that T extends Comparable. why does it complain about item.compareTo(user) ?
    public void add(T item) {
        int i = 0;
        if (items.size() == 0) {
            // first element
            items.add(item);
        } else {
            for (; i < items.size(); i++) {
                T user = items.get(i);
                if (item.compareTo(user) <= 0) {
                    items.add(i, item);
                    break;
                }
            }
            // if we have not added anything inside the list, add to the end
            items.add(item);
        }
        final ListDataEvent listDataEvent = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0, items.size());
        for (ListDataListener listDataListener : changeListeners) {
            listDataListener.intervalRemoved(listDataEvent);
        }
    }

    public void remove(T item) {
        int i = items.indexOf(item);
        items.remove(i);
        final ListDataEvent listDataEvent = new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, 0, items.size());
        for (ListDataListener listDataListener : changeListeners) {
            listDataListener.intervalRemoved(listDataEvent);
        }
    }
}

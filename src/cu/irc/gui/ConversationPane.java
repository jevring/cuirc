package cu.irc.gui;

import cu.irc.commands.CommandHandler;
import cu.irc.net.ServerConnection;
import cu.irc.net.MessageListener;
import cu.irc.net.User;

import java.util.List;
import java.util.Collections;

/**
 * This panel handles conversations.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-16 21:25:06
 */
public abstract class ConversationPane extends IOPane implements ConnectedPane, MessageListener, Window {
    protected final ServerConnection serverConnection;
    protected final String name;

    protected ConversationPane(String source, CommandHandler commandHandler, ServerConnection serverConnection, String title) {
        super(title, commandHandler);
        this.serverConnection = serverConnection;
        this.name = source;

        serverConnection.getMessageDispatcher().addMessageListener(source, this);
    }

    public void message(User source, String message) {
        append(getTimestamp());
        append(" <");
        // _todo: include channel status token, like op or voice (we need the user object for that. where do we get that from?)
        // this applies only if we are a channel. How do we know? we're not going to check this instanceof ChannelPane.
        // we SHOULD override this method, but then we're back to the same place where we were before extending the panel
        // or we simply ask for the channel mode for this, which will always be ""!!!!!!!!!!!!!!
        // good, there we have it, a nice solution
        append(source.getModes(name), yellow);
        if (source.getNick().equals(serverConnection.getMe().getNick())) {
            // color it differently if it is us
            append(source.getNick(), yellow);
        } else {
            append(source.getNick(), paleblue);
        }
        append("> ");
        append(message);
        append("\n");
    }

    public String getName() {
        return name;
    }

    public ServerConnection getServerConnection() {
        return serverConnection;
    }

    public List<Window> getChildren() {
        return Collections.emptyList();
    }

    @Override
    public String toString() {
        return title;
    }
}

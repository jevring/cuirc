package cu.irc.gui;

import cu.irc.commands.CommandHandler;

import javax.swing.*;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import java.awt.*;
import java.beans.PropertyVetoException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.util.*;
import java.util.List;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-05 23:16:29
 */
public class Client extends JFrame implements WindowManager, Window {
    private final String title = "cuirc";
    private final JDesktopPane desktop;
    private Point defaultWindowLocation = new Point(10, 10);
    private Dimension defaultWindowSize = new Dimension(400, 300);
    private final CommandHandler commandHandler = new CommandHandler();
    private final List<Window> servers = new ArrayList<Window>();
    private final JTree windowHierarchy;
    private final WindowTreeModel treeModel;

    public Client() throws HeadlessException {
        setTitle(title);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JSplitPane treeAndDesktop = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);
        add(treeAndDesktop);

        // create tree
        treeModel = new WindowTreeModel(this);
        windowHierarchy = new JTree(treeModel);
        windowHierarchy.setShowsRootHandles(true);
        windowHierarchy.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        windowHierarchy.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                try {
                    final Object o = e.getPath().getLastPathComponent();
                    if (o instanceof JInternalFrame) {
                        JInternalFrame internalFrame = (JInternalFrame) o;
                        internalFrame.setSelected(true);
                        internalFrame.setIcon(false);
                    }
                } catch (PropertyVetoException e1) {
                    e1.printStackTrace();
                }
            }
        });
        /* todo: set the icon depending on the type of node (server/channel/conversation)
        windowHierarchy.setCellRenderer(new DefaultTreeCellRenderer() {
            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

                if (a) {
                    setIcon(resentMessageIcon);
                } else {
                    setIcon(MetalIconFactory.getTreeLeafIcon());
                }
                return this;
            }
        });
        */
        treeAndDesktop.setLeftComponent(new JScrollPane(windowHierarchy));

        // create desktop/mdi pane
        desktop = new JDesktopPane();
        desktop.setBackground(Color.LIGHT_GRAY);
        treeAndDesktop.setRightComponent(new JScrollPane(desktop));

        // todo: recall these from settings on disk
        setSize(1280,1024);
        setLocation(500,500);

        // give us a starting window to work with
        final ServerPane server = new ServerPane(commandHandler, this);
        servers.add(server);
        addWindow(server);

        // todo: recall last connection and connect there again
    }

    /**
     * Adds a window to the desktop.
     *
     * @param window the window that should reside in the window we're adding to the desktop.
     */
    public void addWindow(IOPane window) {
        window.setVisible(true);
        window.setSize(defaultWindowSize);
        window.setLocation(defaultWindowLocation); // todo: if we already have a window in that position, offset this one a little bit.
        window.addPropertyChangeListener(IOPane.TITLE_PROPERTY, new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                // update the tree view to reflect the new title
                treeModel.modelChanged();
            }
        });
        // todo: If we are maximized, maximize the window (can we maximize so that it looses its title bar?)
        desktop.add(window);
        treeModel.modelChanged();

        // expand the entire tree
        int row = 0;
        while (row < windowHierarchy.getRowCount()) {
            windowHierarchy.expandRow(row);
            row++;
            // todo: this doesn't actually expand the whole thing, fix it so that it does
        }

        // select the new window
        try {
            window.setSelected(true);
            window.requestFocusInWindow();
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        // todo: we need to be able to move the slider to encompass the size of the tree, whatever it may be
    }

    public void removeWindow(IOPane window) {
        window.dispose();
        treeModel.modelChanged();
    }

    public List<Window> getChildren() {
        return servers;
    }

    @Override
    public String toString() {
        return title;
    }

    public static void main(String[] args) {
        Client c = new Client();
        c.setVisible(true);
    }
}

// todo: toolbar (icons)
// todo: menu bar

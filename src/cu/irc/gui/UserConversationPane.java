package cu.irc.gui;

import cu.irc.commands.CommandHandler;
import cu.irc.net.ServerConnection;
import cu.irc.net.User;

/**
 * This panel handles conversation with another user.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-16 23:14:11
 */
public class UserConversationPane extends ConversationPane {
    protected UserConversationPane(User peer, CommandHandler commandHandler, ServerConnection serverConnection) {
        super(peer.getNick(), commandHandler, serverConnection, "Conversation with " + peer.getNick() + " [" + peer.toString() + "]");
    }
}

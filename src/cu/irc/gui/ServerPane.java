package cu.irc.gui;

import cu.irc.net.*;
import cu.irc.commands.CommandHandler;

import java.util.*;
import java.io.IOException;

/**
 * An I/O pane that represents a server connection.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-07 22:31:07
 */
public class ServerPane extends IOPane implements ConnectedPane, Window, Connectable {
    private final List<Window> windows = new ArrayList<Window>(); // todo: maybe make this sorted (resort on each addition) Window doesn't have a getName() it probably should (or maybe sort on title)
    private final Map<String, ChannelPane> channelsByName = new HashMap<String, ChannelPane>();
    private ServerConnection serverConnection = null;
    private final WindowManager windowManager;
    private static final String defaultTitle = "Disconnected"; // if it's not static, we can't pass it to the super constructor

    public ServerPane(CommandHandler commandHandler, WindowManager windowManager) {
        super(defaultTitle, commandHandler);
        this.windowManager = windowManager;
    }

    // a server pane gets a ServerConnection by someone invoking /connect(or server) hostname:port in the window
    // or /server -new hostname:port in a different window, which spawns a new ServerPane and invokes connect() right away

    public void connect(String hostname, int port, boolean ssl) throws IOException {
        serverConnection = new ServerConnection(hostname, port, ssl);
        // default catch-all event. For everything not otherwise caught.
        serverConnection.getEventDispatcher().addEventHandler("*", new EventHandler() {
            public void handleEvent(String eventType, String event) {
                append(getTimestamp() + " ");
                appendln(event);
            }
        });
        // create a new channel window when we join a channel
        serverConnection.getEventDispatcher().addEventHandler("JOIN", new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :captain_!captain@ip-62-235-216-20.dsl.scarlet.be JOIN :#politics
                String channelName = event.substring(event.indexOf(':', 1) + 1);
                try {
                    User u = new User(event.substring(1, event.indexOf(' ')));
                    if (u.getNick().equals(getServerConnection().getMe().getNick())) {
                        // this is us joining a channel
                        ChannelPane cp = new ChannelPane(channelName, commandHandler, serverConnection);
                        windows.add(cp);
                        channelsByName.put(channelName, cp);
                        windowManager.addWindow(cp);

                        // we have to request the MODE ourselves
                        getServerConnection().send("MODE " + channelName);
                        getServerConnection().send("WHO " + channelName); // find out who everyone really is
                    }  else {
                        // this is someone else joining one of our channels
                        // :seigemann!~seigemann@c1DC400C3.dhcp.bluecom.no JOIN :#openbsd

                        // note: we have to do this here, and not in the ChannelPane, because otherwise we run in to
                        // ConcurrentModificationExceptions, because we would be registering for the JOIN event
                        // while we are already iterating over the handlers for the JOIN event.

                        ChannelPane cp = channelsByName.get(channelName);
                        // this is ugly, but it's ok in a sense, because a join is actually an external action,
                        // and thus should be doable from the outside
                        cp.join(u);
                        // todo: add this user to the ServerConnection.users as well, with full information
                    }
                } catch (IllegalArgumentException e) {
                    System.err.println("Illegal format, event (including userhost) did not match userhost format: " + event);
                }
            }
        });
        serverConnection.getEventDispatcher().addEventHandler("PART", new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // if we (our nickname) part from a channel, remove that channel from out list(s) of channels
                // :mrbob!captain@ip-62-235-216-20.dsl.scarlet.be PART #keklol
                String channelName = event.split("\\s+")[2];
                User u = new User(event.substring(1, event.indexOf(' ')));
                if (u.equals(serverConnection.getMe())) {
                    // if I left a channel, remove that channel
                    ChannelPane cp = channelsByName.remove(channelName);
                    windows.remove(cp);
                    windowManager.removeWindow(cp); // updates model and stuff too
                }
            }
        });
        // todo: register a handler: if this window is closed, disconnect the connection, destroy all windows, destroy this, create a new empty one
        serverConnection.getMessageDispatcher().setIncomingConversationHandler(new IncomingConversationHandler(){
            public void handleIncomingConversation(User user, String message) {
                // _todo: where do we add the users to the serverConnection?
                // that's already been done in the PRIVMSG handler in the MessageDispatcher
                ConversationPane cp = new UserConversationPane(user, commandHandler, serverConnection);
                cp.message(user, message);
                windows.add(cp);
                windowManager.addWindow(cp);
                getServerConnection().send("WHOIS " + user.getNick());
                // todo: make a WHOIS handler
            }
        });
        serverConnection.connect();
        serverConnection.addConnectionEventListener(ServerConnectionEventListener.State.DISCONNECTED, new ServerConnectionEventListener() {

            public void serverConnected(ServerConnection serverConnection) {
                // not used.
            }

            public void serverDisconnected(ServerConnection serverConnection) {
                // _todo: close all channel windows (via the window manager), change title
                // actually, do NOT close all windows, just mark them unusable. unable to receive new input (other than commands. no messages)
                // that way, if we reconnect again, all history and stuff will not be lost.
                // we should, however, clean out the nick list

            }
        });
        setTitle("Connected to " + serverConnection.getEndPoint() + " (" + serverConnection.getMe().getNick() + ")"); // todo + network and some other stats as well
        // todo: we should use the same updateTitle() thing here as in ChannelPane
        // _todo: inform the tree that it has to update too (somehow)
        // that is done by a property listener on the window already

        // _todo: actually, this class shouldn't be in charge of connecting. that should probably be the CommandHandler
        // maybe we should just connect and then pass this ServerPane (implementing some interface) as an OutputVessel to the ServerConnection
        // that way the server can respond back, and the CommandHandler can handle the commands
        // or maybe the CommandHandler just redirects to us, saying "go ahead, this is where you should connect to".
        // that's probably better. then the COmmandHandler is simple and only does dispatch

        requestFocusInWindow();
        /*
        todo: handle WHOIS to whatever window is currently the visible one
        irc send: WHOIS captain_
        irc recv: :irc.choopa.net 311 mrbob captain_ captain ip-62-235-216-20.dsl.scarlet.be * :captain
        irc recv: :irc.choopa.net 319 mrbob captain_ :#keklol #cupre
        irc recv: :irc.choopa.net 312 mrbob captain_ irc.choopa.net :60 percent of the time it works every time
        irc recv: :irc.choopa.net 338 mrbob captain_ 62.235.216.20 :actually using host
        irc recv: :irc.choopa.net 317 mrbob captain_ 0 1245187851 :seconds idle, signon time
        irc recv: :irc.choopa.net 318 mrbob captain_ :End of /WHOIS list.

         */

        // todo: generally do event handling to the currently active window
    }

    public ServerConnection getServerConnection() {
        return serverConnection;
    }

    public List<Window> getChildren() {
        return windows;
    }

    @Override
    public String toString() {
        // this is used in the tree representation
        if (serverConnection != null) {
            return serverConnection.toString();
        } else {
            return defaultTitle;
        }
    }
}

package cu.irc.gui;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeModelEvent;
import java.util.List;
import java.util.LinkedList;

/**
 * A tree model for the window hierarchy.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-10 17:57:53
 */
public class WindowTreeModel implements TreeModel {
    private final List<TreeModelListener> treeModelListeners = new LinkedList<TreeModelListener>();
    private final Window root;

    public WindowTreeModel(Window root) {
        this.root = root;
    }

    public Object getRoot() {
        return root;
    }

    public Object getChild(Object parent, int index) {
        Window w = (Window)parent;
        return w.getChildren().get(index);
    }

    public int getChildCount(Object parent) {
        Window w = (Window)parent;
        return w.getChildren().size();
    }

    public boolean isLeaf(Object node) {
        Window w = (Window)node;
        return w.getChildren().size() == 0;
    }

    public void valueForPathChanged(TreePath path, Object newValue) {
        System.out.println("value for path " + path + " changed to " + newValue);
    }

    public int getIndexOfChild(Object parent, Object child) {
        Window w = (Window)parent;
        return w.getChildren().indexOf((Window)child);
    }

    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.add(l);
    }

    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.remove(l);
    }

    /**
     * This method needs to be invoked when the model changes. Otherwise the tree
     * that uses the model will not be updated.
     *
     */
    public void modelChanged() {
        // we might as well pass the root, which updates the whole tree
        TreeModelEvent e = new TreeModelEvent(this, new Object[] {root});
        for (TreeModelListener tml : treeModelListeners) {
            tml.treeStructureChanged(e);
        }
    }
}

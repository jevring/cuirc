package cu.irc.gui;

import cu.irc.commands.CommandHandler;
import cu.irc.net.*;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.text.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Creates a channel pane. This is a normal IOPane that also has a list of users.
 *
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-07 22:12:13
 */
public class ChannelPane extends ConversationPane {
    private final String name;
    private final SortedListModel<String> nicks;
    private String topic = null;
    private String modes = ""; // todo: since +b is a mode, we shouldn't keep them in just a list like this.
    // todo: keys, etc
    // todo: optional logging

    public ChannelPane(String channelName, CommandHandler commandHandler, ServerConnection serverConnection) {
        super(channelName, commandHandler, serverConnection, channelName); // input and output
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // do nothing. the window manager will remove it
        this.name = channelName;
        // todo: if we want information about the user, just ask the ServerConnection for the full User object based on nick
        
        nicks = new SortedListModel<String>();
        JList nickList = new JList(nicks);
        // this becomes the default width of each cell
        nickList.setPrototypeCellValue("MMMMMMMMMMMM"); // todo: use 005.NICKLEN to determine the size of this box (9 on EFNet)
        nickList.setBackground(darkerblue);
        nickList.setForeground(foreground);
        add(new JScrollPane(nickList), BorderLayout.EAST);

        // todo: register closing event handler, so that we can leave the channel when the window is closed
        

        // todo: register a popup menu for users and stuff
        registerChannelEventHandlers();

        this.addInternalFrameListener(new InternalFrameAdapter() {
            @Override
            public void internalFrameClosing(InternalFrameEvent e) {
                getServerConnection().send("PART " + name);
                // todo: when this happens (or we create a server-wide trigger for it), we have to remove this from the tree
                // todo: deregister all handlers (hard), close the window (simple)
                // todo: actually, this should be done when the server receives a PART for this window
                // actually, just remove it from the server and we should be golden
                // or maybe, if we use SoftReferences, we don't have to do anything
                // however, soft references doesn't mean that things get garbage collected directly, only that it CAN get garbage collected
            }
        });
    }

    private void registerChannelEventHandlers() {
        serverConnection.getEventDispatcher().addEventHandler(ReplyCodes.RPL_NAMREPLY, new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :irc.choopa.net 353 captain_ @ #cubnc :captain_ _ndrs @nuked OaSe renols sMk_ anonym F|oFF roae rx @_bacon seven- @argv re-boot @gl xchylde Acido kapsel tittof freezy3k
                // check if it is for us. if it isn't, just do nothing
                String[] parts = event.split("\\s+", 6);
                if (name.equals(parts[4])) {
                    String nickstring = event.substring(event.indexOf(':', 1) + 1);
                    nicks.add(nickstring.split(" ")); // only split up the actual nicks
                    // note that we get multiple of these messages, so it needs to be add, not set.
                    updateTitle();
                }
            }
        });
        serverConnection.getEventDispatcher().addEventHandler(ReplyCodes.RPL_TOPIC, new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :irc.choopa.net 332 mrbob #cubnc :http://sf.net/projects/cubnc | The new home of cubnc: http://www.cutools.net | cubnc 2.0-beta available : http://www.cutools.net/forum/viewtopic.php?f=4&t=8
                // check if it is for us. if it isn't, just do nothing
                String[] parts = event.split("\\s+", 5);
                if (name.equals(parts[3])) {
                    topic = event.substring(event.indexOf(':', 1) + 1);
                    append(getTimestamp() + " ");
                    appendln("Topic set to: " + topic);
                    updateTitle();
                }
            }
        });
        serverConnection.getEventDispatcher().addEventHandler(ReplyCodes.CUSTOM_RPL_TOPICSETBY, new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :irc.choopa.net 333 captain_ #channel ident!nick@host timeinmssincetheepoch
                String[] parts = event.split(" ");
                if (name.equals(parts[3])) {
                    String setBy = parts[4];
                    Date setAt = null;
                    try {
                        setAt = new Date(Long.parseLong(parts[5]));
                    } catch (NumberFormatException e) {
                        System.err.println("Could not parse long: " + e.getMessage());
                    }
                    append(getTimestamp() + " ");
                    appendln("Topic set by " + setBy + " at " + setAt);
                }
            }
        });
        serverConnection.getEventDispatcher().addEventHandler(ReplyCodes.RPL_CHANNELMODEIS, new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :irc.choopa.net 324 captain_ #drftpd-devel +stnl 54
                String[] parts = event.split(" ", 5);
                if (name.equals(parts[3])) {
                    modes = parts[4];
                    append(getTimestamp() + " ");
                    appendln("Channel modes: " + modes);
                    // todo: is this the same response as someone setting the channel mode? if not, what is that then?
                    updateTitle();
                }
            }
        });
        serverConnection.getEventDispatcher().addEventHandler("PART", new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // if someone from this channel parts, echo it and remove him/her from the nick list
                // :captain_!captain@ip-62-235-216-20.dsl.scarlet.be PART #keklol
                String channelName = event.substring(event.indexOf(':', 1) + 1);
                if (name.equals(channelName)) {
                    User u = new User(event.substring(1, event.indexOf(' ')));
                    nicks.remove(u.getNick());
                    append(getTimestamp() + " ");
                    appendln(u + " left the channel");
                    updateTitle();
                }
            }
        });
        serverConnection.getEventDispatcher().addEventHandler("MODE", new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :moderatum!moderatum@chat.moderator MODE #politics +v captain_
                // todo: add MODE handling for nicks
                // todo: there is more to the MODE command than we might think. look over the documentation, this probably needs to be on a server level.
            }
        });
    }

    /**
     * Updates the title of the window based on a template (future) and the information available.
     */
    private void updateTitle() {
        setTitle(name + " [" + nicks.getSize() + "][" + modes + "]" + (topic == null ? ": No topic set" : ": " + topic));
    }

    /**
     * Invoke this when a user joins this channel.
     *
     * @param user the user that joined
     */
    public void join(User user) {
        nicks.add(user.getNick());
        append(user.toString(), yellow);
        appendln(" joined the channel", foreground);
    }
}

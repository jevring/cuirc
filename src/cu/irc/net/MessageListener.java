package cu.irc.net;

/**
 * Implementations of this interface can listen to events that happen in a specified channel.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-09 22:10:40
 */
public interface MessageListener {
    public void message(User source, String message);
    // todo: mode, kick, topic, etc.
}

package cu.irc.net;

/**
 * Handles messages sent from unknown senders.
 * This is created to abstract any interface from the irc implementation.
 * If you are using a graphical user interface, for instance, this is what
 * you would use to register something that can create message and channel
 * window with the {@link cu.irc.net.MessageDispatcher} using {@link MessageDispatcher#setIncomingConversationHandler(IncomingConversationHandler)}
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-16 21:56:02
 */
public interface IncomingConversationHandler {
    /**
     * Starts a new conversation with the user.
     *
     * @param user
     * @param message the first message of the conversation.
     */
    void handleIncomingConversation(User user, String message);

    // todo: what about the same from our side? it would be nice if we could utilize the same component
    // actually, just doing /msg ... should probably just show the input (formatted) in the current pane
    // it's only when you open a new window from our side, or something. maybe that can be a policy
}

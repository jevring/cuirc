package cu.irc.net;

import java.net.Socket;
import java.net.InetSocketAddress;
import java.net.InetAddress;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.HashMap;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-07 23:38:52
 */
public class ServerConnection implements Runnable {
    private Socket socket;
    private final String hostname;
    private final int port;
    private final boolean ssl;
    private final Map<ServerConnectionEventListener.State, List<ServerConnectionEventListener>> connectionEventListeners = new HashMap<ServerConnectionEventListener.State, List<ServerConnectionEventListener>>();
    private final Map<String, User> users = new HashMap<String, User>();
    private final EventDispatcher eventDispatcher = new EventDispatcher();
    private final MessageDispatcher messageDispatcher = new MessageDispatcher(eventDispatcher, this);
    private boolean debug = true;
    private OutputStreamWriter out;
    private BufferedReader in;
    private Thread reader;
    private User me;

    public ServerConnection(String hostname, int port, boolean ssl) {
        this.hostname = hostname;
        this.port = port;
        this.ssl = ssl;
        // todo: we need a connectionTimeout and an idleTimeout
        me = new User("mrbob", null, null); // todo: this needs to be done better

        eventDispatcher.addEventHandler(ReplyCodes.RPL_USERHOST, new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :irc.choopa.net 302 captain_ :captain_=+captain@62.235.216.20
                String ident = event.substring(event.indexOf('=') + 2, event.indexOf('@'));
                String host = event.substring(event.indexOf('@') + 1);
                me.setIdent(ident);
                me.setHost(host);
                // todo: we must know that 'me' references a User object registered in the users map
            }
        });
        eventDispatcher.addEventHandler(ReplyCodes.CUSTOM_RPL_WELCOME, new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :irc.choopa.net 001 captain_ :Welcome to the EFNet Internet Relay Chat Network captain_
                // once we know we're connected, we can send this
                send("USERHOST " + me.getNick());
            }
        });
        eventDispatcher.addEventHandler(ReplyCodes.RPL_WHOREPLY, new EventHandler() {
            public void handleEvent(String eventType, String event) {
                // :irc.choopa.net 352 captain_ #pzs-ng captain ip-62-235-216-20.dsl.scarlet.be irc.choopa.net captain_ H :0 captain
                String[] parts = event.split(" ");
                String nick = parts[3];
                String ident = parts[4];
                String host = parts[5];
                User u = new User(nick, ident, host);
                getUser(u); // this will set the user if it doesn't exist, and update it if it does
            }
        });
    }

    public void connect() throws IOException {
        if (ssl) {
            // todo: ssl connect
        } else {
            socket = new Socket();
            socket.connect(new InetSocketAddress(hostname, port), 10000);
        }
        out = new OutputStreamWriter(socket.getOutputStream());
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        // todo: that's right, build in an ident server as well! (only when connecting)
        send("NICK " + me.getNick());
        send("USER " + me.getNick() + " notused.com " + hostname + " :" + me.getNick()); // this last thing should be "real name", but since we don't have it yet...

        fireConnectedEvent();

        reader = new Thread(this, "server line reader");
        reader.setDaemon(true);
        reader.start();

    }

    public void close() throws IOException {
        socket.close(); // this will also terminate the listening thread
    }

    public void run() {
        String line;
        try {
            while ((line = in.readLine()) != null) {
                if (debug) {
                    System.out.println("irc recv: " + line);
                }
                // this is our inverse keepalive
                if (line.startsWith("PING")) {
                    send("PONG " + line.substring(5));
                } else {
                    eventDispatcher.dispatchEvent(line);
                }
            }
        } catch (IOException e) {
            fireDisconnectedEvent();
        }
    }

    /**
     * Sends raw commands to the server, unformatted.
     * @param command the command to send.
     */
    public void send(String command) {
        // I fucking wrote it here, but I fucking forgot, this is retarded!
        // ALWAYS FLUSH YOUR GODDAMNED STREAMS!
        if (command.length() > 509) {
            System.out.print("Trimming command from '" + command + "'");
            command = command.substring(0, 519);
            System.out.println("to '" + command + "'");
            // todo: dunno if trimming is the best, but it'll have to do for now
        }
        if (debug) {
            System.out.println("irc send: " + command);
        }
        try {
            out.write((command+"\r\n").toCharArray());
            out.flush();
        } catch (IOException e) {
            fireDisconnectedEvent();
        }
    }

    /**
     * Invoked internally when the connection is terminated for some reason.
     * Perhaps in the future, we might include a reason or something
     */
    private void fireDisconnectedEvent() {
        List<ServerConnectionEventListener> listeners = connectionEventListeners.get(ServerConnectionEventListener.State.DISCONNECTED);
        if (listeners != null) {
            for (ServerConnectionEventListener listener : listeners) {
                listener.serverDisconnected(this);
            }
        }
    }

    /**
     * Invoked internally when the the connection has been established.
     * Perhaps in the future, we might include a reason or something
     */
    private void fireConnectedEvent() {
        List<ServerConnectionEventListener> listeners = connectionEventListeners.get(ServerConnectionEventListener.State.CONNECTED);
        if (listeners != null) {
            for (ServerConnectionEventListener listener : listeners) {
                listener.serverConnected(this);
            }
        }
    }

    public void addConnectionEventListener(ServerConnectionEventListener.State state, ServerConnectionEventListener listener) {
        // todo: javadoc (mention that it is not thread safe)
        List<ServerConnectionEventListener> listeners = connectionEventListeners.get(state);
        if (listeners == null) {
            listeners = new LinkedList<ServerConnectionEventListener>();
            connectionEventListeners.put(state, listeners);
        }
        listeners.add(listener);
    }

    public User getMe() {
        return me;
    }

    public EventDispatcher getEventDispatcher() {
        return eventDispatcher;
    }

    public MessageDispatcher getMessageDispatcher() {
        return messageDispatcher;
    }

    @Override
    public String toString() {
        return socket.getInetAddress().getHostName() + ":" + socket.getPort();
    }

    public InetAddress getEndPoint() {
        return socket.getInetAddress();
    }

    /**
     * Gets a user from the server connection. If a user with this nick already exists, that user object will be
     * returned. If the provided user has more information that the existing user, the existing user gets this
     * data.
     *
     * @param user a user object with at least the nickname set.
     * @return a (potentially preexisting) user with the same nickname as the provided user.
     */
    public User getUser(User user) {
        User preexistingUser = users.get(user.getNick());
        if (preexistingUser == null) {
            // add the user
            users.put(user.getNick(), user);
            return user;
        } else {
            // if it exists, try to update it
            update(preexistingUser, user);
            return preexistingUser;
        }
    }

    /**
     * Updates the {@code preexistingUser} with the information from {@code user} if possible.
     * Such an upgrade is possible if any field is null or empty in the {@code preexistingUser}.
     * This method assumes that none of the users are null.
     *
     * @param preexistingUser the user to be updated.
     * @param user the source of the update.
     * @throws NullPointerException if any of the user objects are null.
     */
    private void update(User preexistingUser, User user) {
        if (user.getIdent() != null && preexistingUser.getIdent() == null) {
            preexistingUser.setIdent(user.getIdent());
        }
        if (user.getHost() != null && preexistingUser.getIdent() == null) {
            preexistingUser.setHost(user.getHost());
        }
        // todo: check the modes
    }
}

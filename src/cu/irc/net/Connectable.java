package cu.irc.net;

import java.io.IOException;

/**
 * Represents something that can be connected.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-13 19:22:35
 */
public interface Connectable {

    /**
     * Connects to the specified resource.
     * @param server the hostname or ip of the machine to connect to.
     * @param port the port on which to connect.
     * @param ssl whether ssl should be enabled or not.
     * @throws IOException if the connection could not be established for some reason.
     */
    public void connect(String server, int port, boolean ssl) throws IOException;
}

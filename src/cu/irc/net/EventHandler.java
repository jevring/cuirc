package cu.irc.net;

/**
 * Handles server events, such as PRIVMSG or KICK or any of the numerical events.
 *
 * todo: use examples for a) a channel abd b) some numerical thing or something. maybe other channel things as well, like kick
 *
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-08 22:50:11
 */
public interface EventHandler {

    /**
     * Handles an event sent from the server. Events are sent as strings and not first-class objects. This is
     * because we have a large amount of different events, so we would get an overflow of classes. Furthermore,
     * it is convenient to have all the event processing, including parsing, done in the same place.
     *
     * @param eventType the type of event. This is useful if an event handler is used for more than one event.
     * @param event a string represending the event from the server.
     */
    public void handleEvent(String eventType, String event);
}

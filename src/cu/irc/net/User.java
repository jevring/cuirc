package cu.irc.net;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;
import java.util.HashMap;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-11 21:09:25
 */
public class User {
    /**
     * 1 = nick
     * 2 = ident
     * 3 = host
     * This includes the potential first colon (':') in case we are ever lazy
     */
    public static final Pattern USERHOST = Pattern.compile(":?(.+)!(.+)@(.+)");
    private String nick;
    private String ident;
    private String host;
    /**
     * Keeps the mode for each of the channels. For example:
     * #cuftpd -> @
     * #cubnc -> @+
     * Since it is rare that people have multiple modes in the same channel,
     * and there is a herarchy between modes (@ > +, for instance), we keep all
     * the modes in a string instead of a list, and always do .charAt(0) to get
     * the primary mode. Modes *should* always be sorted in here.
     * todo: verify if it is actually possible to have multiple modes in a channel
     */
    private Map<String, String> channelModes = new HashMap<String, String>();

    /**
     * Creates a user from the userhost string "username!ident@host".
     *
     * @param userhost the string to create the user from
     * @throws IllegalArgumentException if the provided string could not be parsed.
     */
    public User(String userhost) {
        Matcher m = User.USERHOST.matcher(userhost);
        if (m.matches()) {
            nick = m.group(1);
            ident = m.group(2);
            host = m.group(3); // it it matches is implicitly has 3 groups
        } else {
            throw new IllegalArgumentException("Could not parse string '" + userhost + "' according to pattern '" + USERHOST.pattern() + "'");
        }
    }

    /**
     * Creates a user with the provided nick, ident and host. Ident and host are allowed to be null,
     * but not the nick.
     * @param nick the nickname.
     * @param ident the ident.
     * @param host the hostname or IP.
     * @throws NullPointerException if the nick is null.
     */
    public User(String nick, String ident, String host) {
        if (nick == null) {
            throw new NullPointerException("Users cannot exist without nicknames");
        }
        this.nick = nick;
        this.ident = ident;
        this.host = host;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String toString() {
        return nick + "!" + ident + "@" + host;
    }

    /**
     * Sets the modes of this user in the specified channel.
     * @param channel the channel for which the user has these modes.
     * @param modes the modes of the user in the specified channel.
     * @return the previous modes of the user in the specified channel, if any.
     */
    public String setModes(String channel, String modes) {
        return channelModes.put(channel, modes);
    }

    /**
     * Returns the modes a this user has on a given channel.
     * @param channel the channel.
     * @return the modes a this user has on a given channel.
     */
    public String getModes(String channel) {
        return channelModes.get(channel);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (host != null ? !host.equals(user.host) : user.host != null) return false;
        if (ident != null ? !ident.equals(user.ident) : user.ident != null) return false;
        if (nick != null ? !nick.equals(user.nick) : user.nick != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nick != null ? nick.hashCode() : 0;
        result = 31 * result + (ident != null ? ident.hashCode() : 0);
        result = 31 * result + (host != null ? host.hashCode() : 0);
        return result;
    }
}

package cu.irc.net;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-09 18:30:50
 */
public interface ServerConnectionEventListener {
    // todo: javadoc this, and maybe give it a shorter name
    public enum State {
        CONNECTED, DISCONNECTED
    }
    public void serverConnected(ServerConnection serverConnection);
    public void serverDisconnected(ServerConnection serverConnection);
}

package cu.irc.net;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

/**
 * Responsible for registering listeners for and dispatching events received from a {@link ServerConnection}.
 * 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-13 18:38:10
 */
public class EventDispatcher {
    /**
     * It's String -> List, because we can have multiple listeners for the same event.
     */
    private final Map<String, List<EventHandler>> eventHandlers = new HashMap<String, List<EventHandler>>();

    public EventDispatcher() {

    }

    /**
     * Adds a handler for a certain event. It is *very* important that you do not
     * add an event handler that, when triggered, would add an event handler for
     * the same event. If you do this you will get a {@code ConcurrentModificationException}
     * when executing that handler.
     *
     * @param event the event to add the handler for.
     * @param handler the handler that will handle the event.
     */
    public void addEventHandler(String event, EventHandler handler) {
        // todo: javadoc (mention that it is not thread safe)
        List<EventHandler> handlerList = eventHandlers.get(event);
        if (handlerList == null) {
            handlerList = new LinkedList<EventHandler>();
            eventHandlers.put(event, handlerList);
        }
        handlerList.add(handler);
    }

    /**
     * Dispatches events received from the server to the respective event listeners.
     *
     * @param line the event to be dispatched.
     */
    public void dispatchEvent(String line) {
        int firstSpace = line.indexOf(' ');
        int secondSpace = line.indexOf(' ', firstSpace + 1);
        String eventType = line.substring(firstSpace + 1,  secondSpace);
        List<EventHandler> handlers = eventHandlers.get(eventType);
        if (handlers != null) {
            for (EventHandler handler : handlers) {
                // why do we get a ConcurrentModificationException here? we don't add things to this while we are going through it, do we?
                // well, maybe we do. if we dispatch the JOIN event, then the ChannelPane created is likely to add some events
                // a simple solution is just to add all the applicable handlers to a list, and go over that list and execute the method, but that's kind of ugly
                // also it won't work. what if, when we're going through them, a new event arrives, and then we're in here dispatching anyway

                // to protect us in general, we need to have a concurrent strucutre that holds these handlers.
                // however, since we have one event dispatcher per server, we will never have multithreaded access to these structures
                // thus it should be possible to avoid things like this, while at the same time sticking to a normal collection class.

                handler.handleEvent(eventType, line);
            }
        } else {
            // if no specific event handler can be found, look for generic ones
            handlers = eventHandlers.get("*");
            if (handlers != null) {
                for (EventHandler handler : handlers) {
                    handler.handleEvent(eventType, line);
                }
            }
        }
    }
}

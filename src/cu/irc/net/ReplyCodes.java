package cu.irc.net;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-10 22:46:36
 */
public class ReplyCodes {
    public static final String RPL_USERHOST = "302";
    public static final String RPL_CHANNELMODEIS = "324";
    public static final String RPL_TOPIC = "332";
    public static final String RPL_WHOREPLY = "352";
    public static final String RPL_NAMREPLY = "353";
    public static final String ERR_CHANOPRIVSNEEDED = "482";

    // custom unstandardized codes (not part of the rfc)
    public static final String CUSTOM_RPL_WELCOME = "001";
    public static final String CUSTOM_RPL_CHANNELCREATEDAT = "329";
    public static final String CUSTOM_RPL_TOPICSETBY = "333";
}

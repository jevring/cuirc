package cu.irc.net;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-11 23:37:38
 */
public interface ChannelEventHandler {

    /**
     * Handles events regarding a particular channel.
     * @param channel the channel in question.
     * @param eventType the event type.
     * @param event the event
     */
    public void handleEvent(String channel, String eventType, String event);
}

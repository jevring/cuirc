package cu.irc.net;

import java.util.Map;
import java.util.HashMap;

/**
 * Dispatches messages from channels and users.
 * To receive messages from a channel or a user, register a {@link cu.irc.net.MessageListener} with this class.
 *
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-16 21:49:42
 */
public class MessageDispatcher {
    /**
     * ChannelListeners listen to all events pertaining to a channel.
     * There can only be one listener for a particular channel.
     */
    private final Map<String, MessageListener> messageListener = new HashMap<String, MessageListener>();
    private IncomingConversationHandler incomingConversationHandler = null;

    /**
     * We receive messages by registering for the PRIVMSG event with the {@link cu.irc.net.EventDispatcher} for this connection.
     * @param eventDispatcher the dispatcher to register with to get the PRIVMSG events.
     * @param serverConnection the server connection that the messages are coming from. This is used to register/get user objects.
     */
    public MessageDispatcher(EventDispatcher eventDispatcher, final ServerConnection serverConnection) {
        // this handles dispatching of all channel messages to their respective channels (as well as private messages)
        // _todo: why is this here? why isn't this in the channels themselves?
        // it is here so that the channels only get their channel messages and not all channel messages
        // also, there are other PRIVMSGs than those to channels; for example those to the user directl
        eventDispatcher.addEventHandler("PRIVMSG", new EventHandler() {
            public void handleEvent(String eventType, String event) {
                if (event.charAt(0) == ':') { // this will always be the case
                    event = event.substring(1);
                }
                String[] eventParts = event.split(" ", 4);
                String sender = eventParts[0];
                String recipient = eventParts[2];
                String message = eventParts[3].substring(1); // do substring(1), since otherwise we'll get the leading ':'
                MessageListener listener = messageListener.get(recipient);
                User sendingUser = serverConnection.getUser(new User(sender));
                if (listener != null) {
                    listener.message(sendingUser, message);
                } else {
                    // there is no listener registered for this source. we must create one
                    // this is generally (always?) when you get a new message from someone
                    if (incomingConversationHandler != null) {
                        incomingConversationHandler.handleIncomingConversation(sendingUser, message);
                    }
                }
            }
        });
    }

    /**
     * Adds a listener that listens for messages sent to the specified channel.
     * @param channel the channel to listen to.
     * @param listener the listener that processes the messages
     * @return the previous channel listener, if any, for the specified channel.
     */
    public MessageListener addMessageListener(String channel, MessageListener listener) {
        return messageListener.put(channel, listener);
    }

    /**
     * Registers an {@link IncomingConversationHandler}. This is used when messages from unknown
     * user are received. This can, for instance, be used to register a GUI component
     * that can create new windows.
     *
     * @param incomingConversationHandler a handler for unknown senders.
     */
    public void setIncomingConversationHandler(IncomingConversationHandler incomingConversationHandler) {
        this.incomingConversationHandler = incomingConversationHandler;
    }
}

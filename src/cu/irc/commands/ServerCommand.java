package cu.irc.commands;

import cu.irc.net.Connectable;

import java.io.IOException;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-13 19:21:21
 */
public class ServerCommand implements Command {
    public void execute(String command, Object source, ErrorHandler errorHandler) {
        if (source instanceof Connectable) {
            // /server irc.choopa.net 6667 [false]
            String[] parts = command.split("\\s+");
            String server = parts[1];
            // todo: default to port 6667
            int port = 0;
            try {
                port = Integer.parseInt(parts[2]);
            } catch (NumberFormatException e) {
                errorHandler.handleError("port parameter '" + parts[2] + "' is not a number");
                return;
            }
            boolean ssl = false;
            if (parts.length >= 4) {
                // todo: change this to "-ssl[=(false|true)]" (only "-ssl" inplies true)
                ssl = Boolean.parseBoolean(parts[3]);
            }
            Connectable c = (Connectable)source;
            try {
                c.connect(server, port, ssl);
            } catch (IOException e) {
                errorHandler.handleError("Could not connect to server " + server + ":" + port + " ssl=" + (ssl ? "on" : "off"), e);
            }
        }
        // todo: write the status to the window, like "-> Connecting to $host:$port [(with ssl)}"
    }
}

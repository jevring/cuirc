package cu.irc.commands;

import cu.irc.gui.*;
import cu.irc.net.ServerConnection;

import java.util.Map;
import java.util.HashMap;

/**
 * Handles commands sent from the input fields of various panes.
 * todo: in the future maybe also from gui buttons or scripts etc.
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-08 21:25:45
 */
public class CommandHandler {
    private final Map<String, Command> commands = new HashMap<String, Command>();
    /**
     * Must not contain spaces.
     */
    private final String commandPrefix = "/";

    public CommandHandler() {
        registerCommand("server", new ServerCommand());
        registerCommand("join", new JoinCommand());
        registerCommand("j", new JoinCommand());
        registerCommand("topic", new TopicCommand());
        registerCommand("t", new TopicCommand());
    }

    /**
     * Handles a command. If it is one that matches any of the ones registered, we execute that command.
     * If not, we pass the raw string on to the underlying connection.
     *
     * @param command the command to interpret.
     * @param source the entity that wants to execute the command, for example a {@link cu.irc.gui.ChannelPane} or a {@link cu.irc.gui.ServerPane}.
     * @param errorHandler handles any errors that may arise when executing the command
     */
    public void handleCommand(String command, Object source, ErrorHandler errorHandler) {
        if (command != null) {
            // as most things are likely to be messages sent to a channel, we will check that first
            if (command.startsWith(commandPrefix)) {
                // this is a command
                String trigger = command.substring(commandPrefix.length(), command.indexOf(' ')).toLowerCase();
                Command c = commands.get(trigger);
                if (c != null) {
                    c.execute(command, source, errorHandler);
                } else {
                    errorHandler.handleError("Command unknown: " + trigger);
                }
                // _todo: whenever a command has been executed, write "-> $command" in the window where it was executed
                // if the source is a button (future), then we should write the string above to the server window for the connection in question
                // or maybe we should leave echoing to the commands themselves.
                // yeah, then they can decide what to show and what not to show, like passwords and keys and whatnot
            } else {
                // not a client command. Must be a raw command.
                // if it's a server, send it raw.
                // if it's a channel, send a PRIVMSG to that channel
                if (source instanceof ConnectedPane) {
                    ServerConnection serverConnection = ((ConnectedPane)source).getServerConnection();
                    if (source instanceof ConversationPane) {
                        ConversationPane channel = ((ConversationPane)source);
                        // PRIVMSG #keklol :this is a test
                        serverConnection.send("PRIVMSG " + channel.getName() + " :" + command);
                        // this is now handled in IOPane#showInput()
                        // no, we can't do it there, because then we'll see commands and other stuff too.
                        channel.message(serverConnection.getMe(), command);
                        // todo: in the future, also do crypto
                    } else if (source instanceof ServerPane) {
                        serverConnection.send(command);
                    }
                }
            }
        } else {
            System.err.println("Got null command from " + source);
        }
    }

    /**
     * Register commands like "/server" or "/join". You can only register one command per trigger.
     * If there is already a command registered for this trigger, the method will return that command.
     * Otherwise null.
     *
     * @param trigger the word trigger for the command without a command prefix. For example,
     * if your command prefix is "/" and you want to register the "/server" command, you would
     * pass "server" in here. IMPORTANT: The trigger may NOT contain spaces.
     * @param command the command to execute when the trigger is encountered.
     * @return the previously registered command for this trigger or null if no such command exists.
     */
    public Command registerCommand(String trigger, Command command) {
        return commands.put(trigger.toLowerCase(), command);
    }
}

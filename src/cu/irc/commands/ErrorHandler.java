package cu.irc.commands;

/**
 * Handles errors. This can be passed to things to allow them to report
 * errors without, for instance, throwing exceptions.
 *
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-13 19:44:07
 */
public interface ErrorHandler {
    // todo: throw up an alert box (not a JOptionPane, but the special thing for JInternalFrames)
    // OR just write to the output bit

    public void handleError(String message, Throwable t);
    public void handleError(String message);
    // todo: maybe we should include the source or something here
}

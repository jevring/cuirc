package cu.irc.commands;

import java.util.List;

/**
 * Implementations of this class represent commands. Commands can be things like "/server", "/kick" or "/join".
 * Commands are used by the {@link CommandHandler}.
 *
 * @see {@link CommandHandler#registerCommand(String, Command)} 
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-13 19:02:20
 */
public interface Command {

    /**
     * Executes a command.
     * @param command the string that represents the command.
     * @param source the source of the comand. This can be, for instance,
     * @param errorHandler used to handle any errors that might occur during execution.
     * This is used so that we can a) execute commands asynchronously, and b) redirect
     * error handling to wherever we want.
     */
    public void execute(String command, Object source, ErrorHandler errorHandler);

    // todo: maybe we should do a getDescription() as well
}

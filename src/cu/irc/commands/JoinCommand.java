package cu.irc.commands;

import cu.irc.gui.ConnectedPane;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-13 20:56:34
 */
public class JoinCommand implements Command {
    public void execute(String command, Object source, ErrorHandler errorHandler) {
        // _todo: how do I a) get a ServerConnection to send this to and b) get the *right* server connection?
        // hah, we already had the solution, instanceof ConnectedPane! =)
        if (source instanceof ConnectedPane) {
            // /join channel(s) [key(s)]
            // as long as there are no spaces between the keys or the channels,
            // this command should let you join multiple channels at the same time
            String parameters = command.substring(command.indexOf(' ') + 1);
            ConnectedPane cp = (ConnectedPane)source;
            if (!parameters.startsWith("#")) {
                parameters = "#" + parameters;
            }
            cp.getServerConnection().send("JOIN " + parameters);
        }
    }
}

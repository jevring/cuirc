package cu.irc.commands;

import cu.irc.gui.ChannelPane;

/**
 * This command issues a TOPIC command to the server. The destination of the topic (i.e. the channel)
 * can be omitted if the source is a channel. If the source is a channel and the first word in the parameter
 * list does not start with "#", then the current channel will be used.
 *
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-06-14 22:38:52
 */
public class TopicCommand implements Command {
    public void execute(String command, Object source, ErrorHandler errorHandler) {
        if (source instanceof ChannelPane) {
            ChannelPane cp = (ChannelPane)source;
            String parameters = command.substring(command.indexOf(' ') + 1);
            if (!parameters.startsWith("#")) {
                parameters = cp.getName() + " :" + parameters;
            }
            cp.getServerConnection().send("TOPIC " + parameters);
            // todo: handle the case when "TOPIC [channel]" is sent to inquire about the topic
            // if we get that back, we should a) update the window and b) write it to the window we are in
        }
    }
}
